

library(data.table)
library(tidyverse)

# BM
sceanrios_0 <- fread("Regions/Burnett/Scenarios.csv")
names(sceanrios_0)

APSIM_DT <- loadBothSptDataScenarios("Burnett")
APSIM_DT <- unique(APSIM_DT[,.(APSIMSoil,APSIMPerm,APSIMClim,SOWREG)])
setnames(APSIM_DT,c("SoilName","KS","MetCode","SOWREG"))

APSIM_DT_B <- copy(APSIM_DT[SOWREG != "Maryborough",])

APSIM_DT_M <- copy(APSIM_DT[SOWREG == "Maryborough",])
fwrite(APSIM_DT_M,"Regions/Maryborough/Scenarios.csv")

# WT

(load("/scratch/apsim/RC11_WT_change.RData"))
APSIM_DT1 <- APSIM_DT
(load("/scratch/apsim/RC11_WT_baseline.RData"))
APSIM_DT <- rbindlist(list(APSIM_DT1,APSIM_DT))
APSIM_DT[APSIMSoil == "todd",APSIMSoil:="pingin"]

u(APSIM_DT[MGTZONE == "Barron",.(APSIMClim,APSIMSoil,APSIMPerm,MGTZONE,SOWREG)])

sceanrios_0 <- fread("Regions/WetTropics/Scenarios.csv")
sceanrios_0[SoilName == "todd",SoilName:="pingin"]

#APSIM_DT <- unique(APSIM_DT[!(SOWREG %in% c("Herbert","Tablelands")),.(APSIMClim,APSIMSoil,APSIMPerm,MGTZONE,SOWREG)])
APSIM_DT <- unique(APSIM_DT[!(SOWREG %in% c("Herbert","Tablelands")),.(APSIMClim,APSIMSoil,APSIMPerm,SOWREG)])
setnames(APSIM_DT,c( "MetCode","SoilName","KS","SOWREG"))

sceanrios_1 <- full_join(sceanrios_0,APSIM_DT)

fwrite(sceanrios_1,"Regions/WetTropics/Scenarios.csv")

# Herbert

(load("/scratch/apsim/RC11_WT_change.RData"))
APSIM_DT1 <- APSIM_DT
(load("/scratch/apsim/RC11_WT_baseline.RData"))
APSIM_DT <- rbindlist(list(APSIM_DT1,APSIM_DT))
APSIM_DT[APSIMSoil == "todd",APSIMSoil:="pingin"]

u(APSIM_DT[SOWREG == "Herbert" | MGTZONE == "Herbert",.(APSIMClim,APSIMSoil,APSIMPerm,MGTZONE,SOWREG)])

sceanrios_0 <- fread("Regions/Herbert/Scenarios.csv")
sceanrios_0[SoilName == "todd",SoilName:="pingin"]

#APSIM_DT <- unique(APSIM_DT[!(SOWREG %in% c("Herbert","Tablelands")),.(APSIMClim,APSIMSoil,APSIMPerm,MGTZONE,SOWREG)])
APSIM_DT <- unique(APSIM_DT[SOWREG == "Herbert",.(APSIMClim,APSIMSoil,APSIMPerm,SOWREG)])
setnames(APSIM_DT,c( "MetCode","SoilName","KS","SOWREG"))

sceanrios_1 <- full_join(sceanrios_0,APSIM_DT)
fwrite(sceanrios_1,"Regions/Herbert/Scenarios.csv")

