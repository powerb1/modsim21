# Aggregate FarmAAgg files to SC and Basin
# including yields, applied mill mud, N, irrigation 
# scenario (eg Baseline)

rm(list=ls())
suppressMessages(source("R/Functions.R"))

#plan(multiprocess,workers=4) # run to setup local parallel processes
print(paste("nbrOfWorkers =",nbrOfWorkers()))

# 0 set some constants ------
Region <- "WetTropics" # must match a sub-directory in Regions/
Scenario <- "Baseline"

print(Scenario)

# factor names in order they appear in paddock and farm file names (BU includes IrrigationFreq)
# Factors <- list(PaddockFactors=c("SoilName","P2RClass","MetCode","startYear","region","KS","sowDate"),
#                 FarmFactors=c("SoilName","MetCode","P2RClass","KS"))
Factors <- list(PaddockFactors=c("SoilName","P2RClass","MetCode","startYear","region","KS","sowDate"),
                FarmFactors=c("SoilName","MetCode","P2RClass","KS","APSIMReg"))

#(load("/scratch/apsim/RC10_BU_baseline.RData"))
(load("/scratch/apsim/RC10_WT_baseline.RData"))
APSIM_DT[,FarmFname:=paste0(paste(APSIMSoil,APSIMClim,SIMNAME,APSIMPerm,APSIMReg,sep="$"),".csv")]

# Aggregate farm output to sub-catchment (SC) for each constituent -----------
FarmOutputDir <- "/scratch/apsim/RC11/WT/FarmAAgg/" 
TSoutputDir <- paste0("/scratch/apsim/RC11/BU/SCAnnual/",Scenario,"/")
constituents <- c("Runoff","soil_loss","DINrunoff","Nleached","Drainage","Rain","CaneYield","irrigation") 

# 4.1 check necessary farmAgg files  files exist
#if (!all(unique(APSIM_DT$FarmFname) %in% dir(FarmOutputDir))) stop("Missing farm files.") 
#missingFarms <- setdiff(unique(APSIM_DT$FarmFname),dir(FarmOutputDir))

# 4.2 for each const and SC in APSIM_DT$SUBCATS
SCs <- unique(APSIM_DT$SUBCATS)

#subset for Done SCs 
#SCs <- setdiff(SCs,getCompletedSC(TSoutputDir,SCs,length(constituents)))

x1 <- lapply(SCs,gen_constituentAA,APSIM_DT,AREAS,constituents,FarmOutputDir,Region,TSoutputDir)
#x1 <- future_lapply(SCs,gen_constituentTS,APSIM_DT,AREAS,constituents,FarmOutputDir,Region,TSoutputDir)       



