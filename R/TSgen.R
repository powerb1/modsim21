# generate constituent time series (TS) from DAF managemnt data (APSIM_DT and AREAS)
# 0. get management layer
# 1. Run APSIM for each paddock 
# 2. Farm aggregate paddocks
# 3. Agregate farms to sub-catchment (SC) for each constituent (eg DINrunoff, soil_loss) 

# to do:
# update met files
# move steps 2. and 3. to HPC

# rm(list=ls())

suppressMessages(source("R/Functions.R"))
plan(multisession,workers=36) # run to setup local parallel processes
nbrOfWorkers()

# 0 set some constants ------
Region <- "Burdekin" # must match a sub-directory in Regions/

# factor names in order they appear in paddock and farm file names (BU includes IrrigationFreq)
PaddockFactors <- c("SoilName","P2RClass","MetCode","startYear","region","KS","sowDate","IrrigationFreq")
#PaddockFactors <- c("SoilName","P2RClass","MetCode","startYear","region","KS","sowDate")
FarmFactors <- c("SoilName","MetCode","P2RClass","KS")


# 1. get DAF management layer (currently only on windows) -------------
# library(rgdal)
# SptDataDir <- "../GBRF_scenarios/BU/RC10"
# layer <- "BU_IntSCFU2019_FuncOrders_Cane_Rpits_SoilNutProjs_Diss"
# MgtMatrixPath <- paste0(SptDataDir,"/BU_RC10_Cane_Sim_Mgt_Matrix_SoilNutProjsNoPests.csv")
# constituent <- "SOILNUT"
# scenario <- "baseline"
# RPIT <- "../GBRF_scenarios/BU/BU_RC10_Cane_RPIT_Props_SoilNut.csv"
# MgtData <- getMgtData(Region,SptDataDir,layer,MgtMatrixPath,constituent,scenario,RPIT_Path = RPIT)
# APSIM_DT <- MgtData$APSIM_DT
# AREAS <- MgtData$AREAS
# save(APSIM_DT,AREAS,file="A:/RC10_BU_baseline.RData",compress = T)
(load("/scratch/apsim/RC10_BU_baseline.RData"))
#(load("/scratch/apsim/GBRF_scen_MW_40PerNutCToB.RData")) 


# 2. Run only necessary APSIM files ----------

# 2.1 gen combosDF
#combosDF <- gen_combosDF(Region)


# 2.2 subset combosDF for required Runs
# Farm_df <- as.data.frame(unique(str_split(APSIM_DT[,FarmFname],"[\\$\\.]",simplify = T))[,1:4],
#                          stringsAsFactors = F)
# names(Farm_df) <- FarmFactors
# combosDF <- inner_join(combosDF,Farm_df,by=FarmFactors)


# 2.3 Generate sims 
# simFileDir <- "/scratch/apsim/PBS/MW_sims/"
# simtFileName <- paste0("Regions/", Region, "/template.simt")
# gen_Sims(combosDF,simtFileName,simFileDir,Region,parallel=T)

# 2.4 copy sims and qsub file to PBSDirs
# PBSDirs <- paste0("Mackay_",1:25)
# Sims2PBS(simFileDir,PBSDirs,delPBSDirs = T)

#save.image() # save workspace
#load(".RData") # load workspace

## submit PBS jobs to HPC via ssh 
# for ((i=13; i<=50; i++ ))
# do
# echo MW_$i
# cd /scratch/apsim/PBS/MW_$i
# qsub apsim_pbs.qsub
# done

# 2.5 check paddock paddock runs (eg terminated early)
# fileSize_dt <- rbindlist(future_lapply(paste0("/scratch/apsim/PBS/",PBSDirs),getOutFileSizes))
# fileSize_dt[,lastYears:= getLastYear(fileSize_dt$fileName)]
# fileSize_dt$LastYear <- future_sapply(fileSize_dt$fileName,getLastYear,USE.NAMES = F)

# 3. generate farm output ----------------

# 3.1 check  necessary paddock files exist
PBSDirs <- paste0("Mackay_",1:25)
PaddockOutputDirs <- paste0("/scratch/apsim/PBS/",PBSDirs) # dirs of paddock output
#missingDT <- getMissingFiles(PaddockOutputDirs,u(APSIM_DT$FarmFname),PaddockFactors,FarmFactors) 

# 3.2 aggregate multiple paddock files (startyear and sowdate) to farm files
FarmOutputDir <- "/scratch/apsim/RC11/MW/Farm/" 
freadAPSIMargs <- list(incompleteRuns=paste0(Region,"_incompleteRuns.csv"),
                       dateCol=6,
                       lastDate=as.Date("2020-06-30"))
gen_FarmFiles(PaddockOutputDirs,u(APSIM_DT$FarmFname),FarmOutputDir,Region,PaddockFactors,FarmFactors,freadAPSIMargs) 


# 4.0 Agregate farm output to sub-catchment (SC) for each constituent -----------
#FarmOutputDir <- "/scratch/apsim/RC11/MW/Farm/" 
#TSoutputDir <- "/scratch/apsim/MW_SC/MW_GBRF_40FertCtoB/" #write output
#constituents <- c("Runoff","soil_loss","DINrunoff","Nleached","Drainage","Rain") 

# 4.1 check necessary farm files exist
#if (!all(unique(APSIM_DT$FarmFname) %in% dir(FarmOutputDir))) stop("Missing farm files.") 


# 4.2 for each const and SC in APSIM_DT$SUBCATS
#SCs <- unique(APSIM_DT$SUBCATS)
#x1 <- lapply(SCs,gen_constituentTS,APSIM_DT,AREAS,constituents,FarmOutputDir,Region,TSoutputDir)       

